# README #

dysmiR

29-06-2020

This tutorial shows how to use *dysmiR* to identify genes with trans-effect in the expression of their taret genes (both protein coding genes (PCGs) and microRNAs (miRNAs)). The prediction of these genes combines two types of genomic data: somatic mutations and gene expression.

In brief, this methods asseses the likely association of mutations at TFBSs (associated to a given gene X) with changes on gene expression on gene X network. *dysmiR* is an adaptation of the previously published model [*xseq*](https://www.nature.com/articles/ncomms9554) originally designed for exonic sequences, *dysmiR* was adapted to be used with TFBS mutations and with miRNA-target gene networks.


## Table of contents:

  * [Installation](#installation)
  * [Input files](#input-files)
  * [Quickstart](#quickstart)
  * [Examples](#examples)
  * [FAQ](#faq)

## Installation

Make sure you have installed:

* R (version >= 3.6.1)
* Snakemake (version >= 5.5.4)
* [libcurl](https://curl.haxx.se/libcurl/)

1. Download/Clone the repository

```unix
git clone https://jaimicore@bitbucket.org/jaimicore/dysmir_pipeline.git
  
cd dysmir_pipeline
```

2. Install libcurl library (required by R (*cowplot*)[https://cran.r-project.org/web/packages/cowplot/vignettes/introduction.html] and *plotly*)

```unix

## Debian, Ubuntu
libcurl4-openssl-dev

## Fedora, CentOS, RHEL
libcurl-devel

## Solaris
libcurl_dev

```


3. Install openssl library (required by R (*cowplot*)[https://cran.r-project.org/web/packages/cowplot/vignettes/introduction.html] and *plotly*)

```unix

## Debian, Ubuntu
libssl-dev

## Fedora, CentOS, RHEL
openssl-devel

## Solaris
libssl_dev

## MAC OSX
openssl@1.1

```


4. Install libxml2 library 
```unix

## Debian, Ubuntu
libxml2-dev

## Fedora, CentOS, RHEL
libxml2-devel

## Solaris
libxml2_dev

```


4. Install libxml2 library 
```unix

## Debian, Ubuntu
libxml2-dev

## Fedora, CentOS, RHEL
libxml2-devel

## Solaris
libxml2_dev

```

5. Install pandoc. Follow the instructions this [link](https://pandoc.org/installing.html).



6. Install the required R packages

```unix
Rscript R-scripts/Install_R_libraries_dysmiR.R
``` 
    
The list of all R packages required is listed below, all of them are installed with the previous command:
        
  * amap           
  * biomaRt        (Bioconductor)
  * circlize
  * ComplexHeatmap (Bioconductor)
  * cowplot
  * data.table
  * dplyr
  * DT
  * edgeR          (Bioconductor)
  * enrichR
  * GenomicRanges  (Bioconductor)
  * ggnetwork
  * ggplot2
  * ggpubr
  * ggrepel
  * ggridges
  * ggthemes
  * gridExtra
  * igraph
  * network
  * optparse
  * plotly
  * png
  * reshape2
  * rmarkdown
  * stringr
  * tidyr
  * withr
  * xseq (Customised version provided in this repository)


Or alternatively, you can install them individually.

CRAN packages, from R: 

```R

pkg <- "amap"
install.packages(pkg)
``` 


Bioconductor packages, from R: 

```R
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")

pkg <- "GenomicRanges"
BiocManager::install(pkg)
``` 


*xseq* customized version, from terminal: 

```unix

R CMD INSTALL Other/xseq.tar.xz
``` 


## Input files:

*dysmiR* requires as minimal input: (i) a Gene-Sample mutation matrix, (ii) a Gene expression matrix for all the samples in a cohort, and (iii) a gene association network.

  1. Mutation matrix in BED format **(Mandatory)**
  2. Gene expression matrix **(Mandatory)**
  3. Gene Association network **(Mandatory)**
  
  * sRNA-seq expression matrix (Optional)
  * miRNA-target gene association network (Optional)
  * Copy Number Alteration matrices (Optional)
  * Cis-regulatory elements associated to genes (Optional)
  * miRNA <-> premiRNA table (Optional)





#### Mutation matrix **(Mandatory input)**:

A sample-mutation file in BED-like format, with all samples from the same cohort.

Required columns: 

  1. Chromosome
  2. Start
  3. End
  4. Sample ID

Example:

```unix
chr1    10461   10462   Sample_1
chr1    10471   10472   Sample_2
chr1    10492   10493   Sample_3
chr1    10494   10495   Sample_2
chr1    10601   10602   Sample_4
```





#### Gene expression matrix **(Mandatory input)**:

A gene expression matrix where columns are gene names and rows are the sample IDs.

This matrix can be derived from RNA-seq or alternatively from microarrays.

Example: 

```unix
          A1BG  GGACT A2M A2ML1 A4GALT
Sample_1  2.40  3.04  9.02  0.41  4.27
Sample_2  2.94  3.16  7.44  0.55  5.83
Sample_3  2.62  3.65  6.36  0.19  6.37
Sample_4  2.64  4.93  9.33  0.67  4.48
Sample_5  3.07  4.72  6.08  0.58  4.61

```





#### Gene association network **(Mandatory input)**:

A gene-gene association network, represented as a matrix where the association 
between a pair of protein coding genes is a value ranging from 0 to 1.

Columns:

  1. Gene A
  2. Gene B
  3. Degree of association

Example: 

```unix
TP53  CDKN1A  0.999
TP53  CREBBP  0.999
TP53  EP300   0.999
TP53  FGFR4   0.408
TP53  CGREF1  0.404
TP53  SEPT7   0.401
GATA3 MYB     0.970
GATA3 EP300   0.943
GATA3 NOTCH1  0.925
GATA3 HDAC3   0.726
GATA3 STAT4   0.611
GATA3 CD4     0.470
```

Users may provide their own gene association matrix. In case you don't have such
matrix, we provide in this repository an association matrix directly taken from Ding *et al*.

> Ding, J. et al. Systematic analysis of somatic mutations impacting gene expression in 12 tumour types. Nat Commun (2015). https://doi.org/10.1038/ncomms9554





#### miRNA expression matrix (Optional input):

A miRNA expression matrix where columns are miRNA names  and rows are the sample IDs.

This matrix can be derived from sRNA-seq or alternatively from microarrays.

The column names must be the mature miRNA and its corresponding precursor, separated by '::', example : hsa-let-7a-5p::hsa-let-7a-1.

This notation allows to distinguish between similar mature miRNA derived from different precursors:

  - hsa-let-7a-5p::hsa-let-7a-1
  - hsa-let-7a-5p::hsa-let-7a-2

Example: 

```unix
          hsa-let-7a-5p::hsa-let-7a-1   hsa-let-7a-3p::hsa-let-7a-1   hsa-let-7a-5p::hsa-let-7a-2   hsa-mir-934::hsa-mir-934    hsa-mir-96-3p::hsa-mir-96
Sample_1              13.74                         14.73                         15.82                       6.75                          8.77
Sample_2              11.68                         15.63                         19.43                       6.37                          8.38
Sample_3              15.42                         14.29                         18.99                       8.04                          7.58
Sample_4              15.34                         16.17                         21.34                       7.21                          9.24
Sample_5              15.18                         14.05                         17.27                       7.67                          8.64
``` 





#### miRNA-target gene association network (Optional input):

A miRNA-mRNA association network, represented as a matrix where the association 
between a pair of protein coding genes is a value ranging from 0 to 1.

Columns:

  1. Mature miRNA name
  2. Target gene name
  3. Degree of association

Example: 

```unix
hsa-mir-155-5p    H3F3A   0.99
hsa-mir-155-5p    MAP4K3  0.98
hsa-mir-155-5p    ARID2   0.98
hsa-mir-155-5p    BCAT1   0.69
hsa-mir-155-5p    PGAP1   0.38
hsa-mir-155-5p    AZIN1   0.16
hsa-mir-92a-2-5p  PBX1    0.99
hsa-mir-92a-2-5p  RAD1    0.94
hsa-mir-92a-2-5p  COX15   0.89
hsa-mir-92a-2-5p  ONECUT1 0.84
hsa-mir-92a-2-5p  TGS1    0.62
hsa-mir-92a-2-5p  MAPK1   0.39
```

Users may provide their own miRNA-mRNA association matrix. In case you don't have such
matrix, we provide in this repository an association matrix derived from *targetScan*.

To reduce false positives, we considered only those interactions when a miRNA targets the same mRNA in at least two independent sites. The association scores were calculated as t_score / 100 where t_score corresponds to the targetScan context++ score percentiles from *targetScan* predictions.

> Agarwal V et al. Predicting effective microRNA target sites in mammalian mRNAs. Elife (2015). http://dx.doi.org/10.7554/eLife.05005





#### Copy-number call and log2 ratio matrices (Optional input):

Copy-number alteration (CNA) estimates computed from ASCAT or GISTIC and represented a matrix
where columns are gene names and rows are the sample IDs.

The values in the CNA-call matrix  correspond to:

  - -2: homozygous loss
  - -1: hemizygous loss
  -  0: normal
  -  1: three copies
  -  2: more than three copies

Example CNA-call: 

```unix
          A1BG  GGACT A2M A2ML1 A4GALT
Sample_1    0     0    0    1    -1
Sample_2    2     1    0    0     0
Sample_3    0     0    0    0     2
Sample_4   -2    -2    0    0     1
Sample_5   -1     0    0    0     2

```


Example CNA-log2 ratio: 

```unix
             A1BG   GGACT     A2M   A2ML1   A4GALT
Sample_1    0.045   0.054   0.017   0.538   -0.682
Sample_2    2.984   0.337   0.023   0.054    0.031
Sample_3    0.065   0.065   0.076   0.091    2.462
Sample_4   -2.311  -2.199   0.099   0.0756   1.114
Sample_5   -1.154   0.0983  0.023   0.052    2.238

```

Although these matrices are not mandatory, they are highly recommended to remove
the cis-effect of CNA in gene expression.





#### CRE associated to target genes (Optional input):

A file in BED-like format, with the cis-regulatory elements (CREs) coordinates and 
their target gene.

Required columns: 

  1. Chromosome
  2. Start
  3. End
  4. Associated gene

Example: 

```unix
chr11     5639661      5643497    TRIM6-TRIM34
chr11     5639661      5643497     GC11P005579
chr19    49342101     49342230     GC19P048839
chr19    49342101     49342230     GC19P048838
chr19    49342101     49342230        HSD17B14
chr19    49342101     49342230         PLEKHA4
chr1    109795621    109798890           PSRC1
chr1    109795621    109798890          CELSR2
chr1    109795621    109798890           GSTM3
chr1    109795621    109798890        PIR46363
chr19     5164501      5165138     GC19P005167
```

By default, we use the associated CREs derived from *GeneHancer*, but users may 
choose to use the CREs derived from *STITCHIT*. Alternatively users can provide 
their own associated CREs following the format showed above.

> Fishilevich S, Nudel R et al. GeneHancer: genome-wide integration of enhancers and target genes in GeneCards. Database (2017) https://doi.org/10.1093/database/bax028

> Schmidt F et al. Integrative analysis of epigenetics data identifies gene-specific regulatory elements. bioRxiv (2019) https://doi.org/10.1101/585125





#### TFBSs (Optional input):

A file in BED-like format, with the Transcription Factor Binding Sites (TFBSs) coordinates and 
their associated Transcription Factors.

Required columns: 

  1. Chromosome
  2. Start
  3. End
  4. TF

Example: 

```unix
chr1  540802  540818    RARA
chr1  540824  540834    E2F6
chr1  540832  540843    MYC
chr1  540833  540842    MAX
chr1  540923  540932    ERG
chr1  565259  565279    REST
chr1  565289  565307    MAFK
chr1  565338  565347    RELA
chr1  565371  565380    CEBPB
chr1  565391  565400    ERG
chr1  566728  566737    RELA
chr1  567039  567049    GATA2
```

By default, we use a set of TFBSs with computational and experimental evidences derived from *UniBind*. 
Alternatively users can provide their own set of TFBSs following the format showed above.

> Gheorghe M et al. A map of direct TF–DNA interactions in the human genome. NAR (2018). https://doi.org/10.1093/nar/gky1210



#### miRNA - premiRNA association table (Optional input):

A tab file with the information of the mature miRNAs and their precursors (premiRNA).

Required columns: 

  1. miR: mature miRNA
  2. premiR: precursor (premiRNA)

Example: 

```unix 
          miR           premiR
hsa-let-7a-3p     hsa-let-7a-1
hsa-let-7a-5p     hsa-let-7a-1
hsa-let-7a-5p     hsa-let-7a-2
hsa-let-7a-3p     hsa-let-7a-3
hsa-let-7a-5p     hsa-let-7a-3
hsa-miR-1180-3p   hsa-mir-1180
hsa-miR-1180-5p   hsa-mir-1180
hsa-miR-1181      hsa-mir-1181
hsa-miR-1182      hsa-mir-1182
hsa-miR-1183      hsa-mir-1183
```

By default, we use the names derived from *miRbase v20*, but users can provide 
their own miRNA-premiRNA associations following the format showed above.

An easy way to obtain this information is through the the R/Bioconductor package *miRBaseConverter*.

> Kozomara A and Griffiths-Jones S. miRBase: annotating high confidence microRNAs using deep sequencing data. NAR (2011) https://doi.org/10.1093/nar/gkq1027

> Xu T et al. miRBaseConverter: An R/Bioconductor Package for Converting and Retrieving miRNA Name, Accession, Sequence and Family Information in Different Versions of miRBase. BMC Bioinformatics (2018) http://dx.doi.org/10.1101/407148


## Quickstart

There are two possibilities, depending on the data available:

  - Analysis of cis-regulatory mutations associated with trans effect on **protein-coding genes**.
  - Analysis of cis-regulatory mutations associated with trans effect on **protein-coding AND miRNA genes**.
  
We provided a *config* file with the default parameters, users can adapt it to
use their input files, genome versions and set threshold according to their 
convenience.

The following configuration in the *config.yaml* file will launch both protein 
coding and miRNA analysis.
```unix

RNAseq_tab: data/example_dataset/dysmiR_RNA-seq.tab

miRNAseq_tab: data/example_dataset/dysmiR_sRNA-seq.tab

```

In cases when users have only RNA-seq but not miRNA-seq data, change the 
configuration as in the following example:

```yaml

RNAseq_tab: data/example_dataset/dysmiR_RNA-seq.tab

miRNAseq_tab: ""

```

Once you set the *config.yaml* file, launch rune the *Snakfile* script to launch
the pipeline.

```unix

## Assuming you are in the dysmir_pipeline folder

snakemake 
```

Check the [*Snakemake* documentation](https://snakemake.readthedocs.io/en/stable/index.html) for any question related to *Snakemake*.


## Example

When the pipeline finished succesfully, it produces one report file in html for protein-coding genes and another for miRNA genes.

See an example on these links:

  - Report: [Analysis of protein coding genes](http://hfaistos.uio.no/dysmir_pipeline/html_output/dysmiR_analysis_report_ToyExample_PCG.html) 
  - Report: [Analysis of miRNA genes](http://hfaistos.uio.no/dysmir_pipeline/html_output/dysmiR_analysis_report_ToyExample_miRNA.html)


### Rules launched by the pipeline

  - **Generate_ENSEMBL_list_of_protein_coding_genes** : given a genome version, returns a file with the protein coding genes and their IDs.
  - **Associate_regions_with_target_genes** : associates the TFBS coordinates with a set of cic-regulatory elements and closest TSSs.
  - **Map_mutations_within_TFBSs** : return a BED file with the mutated TFBSs.
  - **Filter_TFBS_associated_to_PCGs** : keep the TFBSs associated to genes considered as potentially dysregulated. Select the target genes whose expression value *v* is +/- 1 standard deviation from the mean of *v* observed in a cohort.
  - **xseq_trans_miRNA** : assesses the likely association of observing mutations in miRNA-associated TFBSs with trans-effect in the expression of the genes connected to the miRNAs.
  - **xseq_trans_PCG** : assesses the likely association of observing mutations in TFBS associated to protein-coding genes with trans-effect in the expression of their connected genes.
  - **select_miRNA_and_draw_plots** : select the relevant miRNAs, their dysregulated targets, and draw multiple plots to visualize the results.
  - **select_PCG_and_draw_plots** : select the relevant protein-coding genes, their dysregulated targets, and draw multiple plots to visualize the results.
  - **GO_GSEA_enrichment_per_cohort** : functional enrichment analysis of the dysregulated genes using [*enrichR*](https://amp.pharm.mssm.edu/Enrichr/).
  - **dysmiR_interactive_report** : generates an interactive report with tables and plots.

![DAG](Other/Examples_README/dag.svg)

Directed acyclic graph (**DAG**) of this pipeline.

Note that these DAG is split in two branches, for the analysis of protein-coding genes (right branch) and miRNAs (left branch). This is the DAG launched when both RNA-seq and miRNA-seq data are available, when only RNA-seq is available, the protein-coding branch will be launched.


## FAQ

### How to convert a table into a *RData* object ?

To avoid loading large tables and share precomputed datasets in the repository,
many input files are provided in form of RData objects.

In this repository, the following files are provided as *RData* objects:

  - data/Gene_CRE_associations/STITCHIT/STITCHIT_gene_CRE_associations_hg19.RData
  - data/Gene_CRE_associations/STITCHIT/STITCHIT_gene_CRE_associations_hg38.RData
  - data/Gene_CRE_associations/GeneHancer/GeneHancer_gene_CRE_associations_hg19.RData
  - data/Gene_CRE_associations/GeneHancer/GeneHancer_gene_CRE_associations_hg38.RData
  - data/miRNA_premiRNA_dictionary/hsa_mir_to_premir.RData
  - data/TFBS/TFBSs_ChIP_eat_enrichment_zone_hg19.RData
  - data/TFBS/TFBSs_ChIP_eat_enrichment_zone_hg38.RData
  - data/TSS/TSS_miRNA_FANTOM5_hg19.RData
  - data/TSS/TSS_miRNA_FANTOM5_hg38.RData
  - data/TSS/TSS_pcg_RefSeq_hg19.RData
  - data/TSS/TSS_pcg_RefSeq_hg38.RData

Users can easily convert a tab-delimited table into a RData objects using the 
following script:

```unix 
R-script/Convert_table_to_Rdata.R -t TSS_miRNA_FANTOM5_hg19.bed
```

Where *-t* correspond to any tab-delimited table. The program returns a file with
the same base name, but with *.RData* as extension.


### How to run specific steps in the snakefile ?

In cases when it is required to run a rule in particular (and therefore all the rules below it), users can adapt the following command:

```unix

snakemake -R select_PCG_and_draw_plots
```

This will launch the rule *select_PCG_and_draw_plots* and its descendant rules (*GO_GSEQ_enrichment_per_cohort* and *dysmiR_interactive_report*), see [DAG](#DAG).


### How to convert a tab-delimited assoiation network in the format required by dysmiR/xseq?

Both protein-coding and miRNA networks are required as *Rdata* objects. The networks are represented as *list* containing *vectors*.

  - Each element of the *list* corresponds to a gene (protein-coding or miRNA).
  - Each element in the *vector* correspond to their associated genes (or targets). The values of the vector are the gene associations (from 0 to 1), the *names* of the vector correspond to the associated gene names.


```r
$RUNX1
 SUV39H1     ETS1    SMAD4    RUNX3   IGFBP3
    0.78     0.95        1     0.84     0.45

$TP53
 RB1   SUMO2  ARID3A    DDB2
   1    0.97    0.91    0.59

```

Users can conver a tab-delimited network (in the same format as was shown in the *Gene association network* section) using the following script:

```unix
Rscript R-script/Convert_df_network_to_xseq_format.R -t Example_network_df.tab
```

This script will produce an Rdata object called *Network_in_xseq_format.RData*. 

In this repository, the following files are provided as xseq networks:

  - data/networks/PCGs/xseq_PPI_network.Rdata
  - data/networks/miRNAs/targetScan_clean_miRNA-mRNA_network.Rdata
