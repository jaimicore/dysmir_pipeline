#####################
## Load R packages ##
#####################
required.libraries <- c("data.table",
                        "GenomicRanges",
                        "optparse")

for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    suppressPackageStartupMessages(library(lib, character.only=TRUE))
  }
}



####################
## Read arguments ##
####################
option_list = list(
  
  make_option(c("-o", "--output_directory"), type="character", default=NULL, 
              help="Output directory to export the tables (Mandatory)", metavar="character"),
  
  make_option(c("-t", "--template_bed"), type="character", default = "NULL", 
              help="BED file used as template to generate random positions. (Mandatory) ", metavar="character"),  
  
  make_option(c("-s", "--side"), type="character", default = "both", 
              help="Insert the positions randomly in [left] side, [right] side or [both].", metavar="character"),  
  
  make_option(c("-m", "--max_distance"), type="numeric", default = 10, 
              help="Max distance where the random mutations are generated, relative to the original coordinates in the template. [Default \"%default\"] ", metavar="number"),
  
  make_option(c("-n", "--min_distance"), type="numeric", default = 10, 
              help="Max distance where the random mutations are generated, relative to the original coordinates in the template. [Default \"%default\"] ", metavar="number"),

  make_option(c("-i", "--new_ids"), type="numeric", default = 0, 
              help="Generate unique identifiers based on the given column. [Default \"%default\"] ", metavar="number")
  
  );
message("; Reading arguments from command line")
opt_parser = OptionParser(option_list = option_list);
opt = parse_args(opt_parser);



########################
## Set variable names ##
########################
results.dir         <- opt$output_directory
template.tab.file   <- opt$template_bed
insert.side         <- opt$side
max.distance        <- as.numeric(opt$max_distance)
min.distance        <- as.numeric(opt$min_distance)
new.ids.col         <- as.numeric(opt$new_ids)


## Example
# results.dir <- "/storage/scratch/TCGA/ICGC/dysmir_pipeline/Test"
# template.tab.file <- "/storage/scratch/TCGA/ICGC/dysmir_pipeline/examples/dysmiR_input_mutations.bed"
# max.distance <- 10
# min.distance <- 1
# insert.side <- "both"
# new.ids.col <- 4



#########################
## Mandatory variables ##
#########################
if (!exists("results.dir")) {
  stop("Missing mandatory argument (Output directory): results.dir ")
  
} else if (!exists("template.tab")) {
  stop("Missing mandatory argument: template.tab ")
  
}


###############################
## Generate random positions ##
###############################
runif.positon <- function(position = 25,
                          max.distance = 10,
                          min.distance = 1,
                          side = "both",
                          iter = 5){
  
  na.pos <- 1
  max.iter <- 0
  while ( na.pos == 1 & max.iter <= iter) {
    
    max.iter <- max.iter + 1
    
    ## Generate a new random positions from a uniform distribution
    ## with limits in the max and min distance relative the position
    shift.pos <- round( runif(1, min = min.distance, max = max.distance) )
    
    
    ## Add direction to the shifted position
    ##
    ## Left : multiply by -1
    ## Right: multiply by +1 (left as it it)
    ## Both : randomly choose -1|+1 and multiply the shift
    if (side == "left") {
      
      shift.pos <- shift.pos * -1
      
    } else if (side == "right") {
      
      shift.pos <- shift.pos * 1
      
    } else if (side == "both") {
      
      shift.pos <- shift.pos * sample(c(-1,1), size = 1, replace = T)
    } else {
      stop("; 'side' parameter not recognized. Options: left|right|both")
    }
    
    
    ## Check that new position is not below to 0
    new.position <- position + shift.pos
    new.position <- ifelse(new.position >= 1, yes = new.position, no = NA)
    
    if (!is.na(new.position)) {
      na.pos <- 0
    }
  }
  
  
  ## Return a warning when after the N iterations the shifted position is lower than 1
  if (is.na(new.position)) {
    warning("; The shifted position of ", position, " is lower than 1 after ", iter," tries: NA returned.")
  }

  
  return(new.position)
}



###################
## Read BED file ##
###################
message("; Reading template BED file: ", template.tab.file)
template.tab <- fread(template.tab.file)

## Select the column from which new IDs will be generated
new.id.col.data <- template.tab[, ..new.ids.col]

## Separate the coordinates and supplementary columns
template.coordinates <- template.tab[,1:3]
template.supp <- template.tab[,-c(1,2,3)]



###############################
## Generate random positions ##
###############################
message("; Generating random positions from a uniform distribution")
shifted.coordinates.start <- vapply(template.coordinates$V2,
                                    runif.positon, max.distance = 10, min.distance = 1, side = "both",
                                    numeric(1))

## Generate shifted coordinates
new.coordinates <- template.coordinates
new.coordinates$V2 <- shifted.coordinates.start
new.coordinates$V3 <- new.coordinates$V2 + 1

## Concat new coordinates with supp columns
new.bed <- cbind(template.coordinates, template.supp)


########################################
## Generate random IDs (if indicated) ##
########################################
if (new.ids.col != 0){
  
  old.ids <- unique(unlist(new.id.col.data))
  new.ids <- paste0("S", sprintf("%04d", 1:length(old.ids)))
  
  ids.dict <- data.frame(Old = old.ids,
                         New = new.ids)
  
  ## Export dictionary
  IDs.dic.file <- file.path(results.dir, "Dictionary_IDs.txt")
  message("; Exporting ID dictionary: ", IDs.dic.file)
  fwrite(ids.dict, file = IDs.dic.file, sep = "\t", row.names = F, col.names = T)
  
  new.bed <- merge(x = new.bed, y = ids.dict, 
                  by.x = names(new.bed)[new.ids.col],
                  by.y = "Old")
  
  new.bed <- new.bed[,-1]
  
  colnames(new.bed)[1:3] <- c("chr", "start", "end")
  new.bed <- GenomicRanges::sort(GRanges(new.bed))
  new.bed <- data.frame(new.bed)[,-c(4,5)]
}


###############################################
## Remove positions with NAs and export them ##
###############################################
message("; Removing positions that were not shifted")
keep.positions <- which(complete.cases(new.bed))
remove.positions <- which(!complete.cases(new.bed))

removed.bed <- template.coordinates[remove.positions,]
new.bed <- new.bed[keep.positions,]

base.input <- basename(template.tab.file)
base.input <- unlist(strsplit(base.input, split = "\\."))

## Export BED file with positions that were not modified
nonshift.bed.file <- file.path(results.dir, paste0(base.input[1], "_non_shifted_positions.", base.input[2]))
message("; Exporting BED file with non-shifted positions: ", nonshift.bed.file)
fwrite(removed.bed, file = nonshift.bed.file, sep = "\t", row.names = F, col.names = F)


############################################
## Export BED file with shifted positions ##
############################################
new.bed.file <- file.path(results.dir, paste0(base.input[1], "_randomly_shifted.", base.input[2]))
message("; Exporting BED file with shifted positions: ", new.bed.file)
fwrite(new.bed, file = new.bed.file, sep = "\t", row.names = F, col.names = F)
