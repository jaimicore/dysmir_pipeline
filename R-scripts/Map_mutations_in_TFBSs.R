#####################
## Load R packages ##
#####################
required.libraries <- c("data.table",
                        "dplyr",
                        "GenomicRanges",
                        "optparse",
                        "tidyr")
for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    suppressPackageStartupMessages(library(lib, character.only=TRUE))
  }
}


####################
## Read arguments ##
####################
option_list = list(
  
  make_option(c("-c", "--cohort_name"), type="character", default = "Cohort1", 
              help="Cohort name. [Default \"%default\"] ", metavar="character"),
  
  make_option(c("-t", "--tfbs_table"), type="character", default = "NULL", 
              help="TFBS table with the following columns: chr, start, end, target, TF (Mandatory). Note: the TFBS must be associated to their target genes.", metavar="character"),
  
  make_option(c("-m", "--mutation_table"), type="character", default = NULL, 
              help="Mutation table with the following columns: chr, start, end, sample_id  (Mandatory).", metavar="character"),

  make_option(c("-o", "--output_directory"), type="character", default=NULL, 
              help="Output directory to export the tables (Mandatory)", metavar="character")
  
);
opt_parser = OptionParser(option_list = option_list);
opt = parse_args(opt_parser);

results.dir <- opt$output_directory
tfbs.tab.file <- opt$tfbs_table
mutation.tab.file <- opt$mutation_table
cohort.name <- opt$cohort_name

## Input files + variables
# tfbs.tab.file <- "/storage/scratch/TCGA/ICGC/dysmir_pipeline/dysmiR_example/results/Associated_TFBSs/TFBS_associated_with_GeneHancer_target_genes_PCG_miRNA_hg19.bed"
# mutation.tab.file <- "/storage/scratch/TCGA/ICGC/dysmir_pipeline/examples/dysmiR_input_mutations.bed"

## Define chromosomes
selected.chrom <- paste0("chr" ,c(1:22, "X", "Y", "MT"))


#########################
## Read mutation table ##
#########################
message("; Reading mutation bed file:", mutation.tab.file)
mutation.tab <- fread(mutation.tab.file)
colnames(mutation.tab) <- c("chr", "start", "end", "sample")

## Select chromosomes
mutation.tab <- mutation.tab %>% 
  dplyr::filter(chr %in% selected.chrom)

## Create a GRanges object with the mutations
message("; Generating a GRanges object with the mutation coordinates")
mutation.tab.gr <- GRanges(mutation.tab)


#########################
## Read mutation table ##
#########################
message("; Reading TFBS bed file: ", tfbs.tab.file)

load(tfbs.tab.file)
head(all.associated.features.tab)
tfbs.tab <- all.associated.features.tab


# tfbs.tab <- fread(tfbs.tab.file)
colnames(tfbs.tab) <- c("chr", "start", "end", "target", "TF")

## Select chromosomes
tfbs.tab <- tfbs.tab %>% 
  dplyr::filter(chr %in% selected.chrom)

## Create a GRanges object with the mutations
message("; Generating a GRanges object with the TFBS coordinates")
tfbs.tab.gr <- GRanges(tfbs.tab)


####################################
## Intersect mutations with TFBSs ##
####################################
message("; Identifying mutations within the associated TFBSs")

## Return a two-column table
## Col 1: Row in Mutation table
## Col 2: Row in TFBS table
overlaps.mut.in.TFBSs <- data.frame(
                          GenomicRanges::findOverlaps(query = mutation.tab.gr,
                                                      subject = tfbs.tab.gr,
                                                      ignore.strand=TRUE))

## Select relevant columns in the matched mutations
matched.mutations <- data.frame(mutation.tab.gr)[overlaps.mut.in.TFBSs$queryHits,]
matched.mutations <- matched.mutations %>% 
                      dplyr::select(seqnames, start, end, sample) %>% 
                      dplyr::rename(chr = seqnames)

## Select relevant columns in the matched TFBSs
matched.TFBSs <- data.frame(tfbs.tab.gr)[overlaps.mut.in.TFBSs$subjectHits,]
matched.TFBSs <- matched.TFBSs %>% 
                  dplyr::select(target, TF)

if( !nrow(matched.mutations) == nrow(matched.TFBSs) ){

  stop("; Error: the number of matched mutations does not correspond to the number of matched TFBSs")
}


##########################
## Prepare output table ##
##########################
message("; Combining mutation and TFBS information")
## Concat the tables, then remove repreated entries
mutated.TFBss <- cbind(matched.mutations,
                       matched.TFBSs) %>% 
                 # head(20) %>% 
                 dplyr::mutate(ID = paste(chr,start,end,sample,target,TF, sep = "_")) %>%  ## ID: temporal variable
                 dplyr::group_by(ID) %>%  ## USe slice to select the first entry of each group
                 dplyr::slice(1) %>% 
                 dplyr::ungroup()

## Remove the temporal variable ID
mutated.TFBss <- within(mutated.TFBss, rm(ID))


#####################################################
## Export tables with mutated TFBSs: PCGs & miRNAs ##
#####################################################
message("; Formating mutation table in order required by xseq")
mutated.TFBss.format <- mutated.TFBss %>% 
  mutate(cohort = cohort.name) %>% 
  dplyr::select(chr, start, end, sample, target, TF, cohort) %>% 
  dplyr::rename(gene = target)

# mutated.TFBss.format <- mutated.TFBss %>% 
#   mutate(effect = "TFBS",
#          mutation_type = "OTHER",
#          cohort = cohort.name) %>% 
#   dplyr::select(sample, target, mutation_type, effect, chr, start, end, TF, cohort) %>% 
#   dplyr::rename(gene = target)


## Select miRNAs
message("; Selecting mutations on TFBSs associated to miRNA genes")
mirna.positions <- grep(mutated.TFBss.format$gene, pattern="^hsa-.*mir-", ignore.case=T, perl = T)
mutated.TFBss.miRNA <- mutated.TFBss.format[mirna.positions,]

## Select PCGs
message("; Selecting mutations on TFBSs associated to protein-coding genes")
mutated.TFBss.PCG <- mutated.TFBss.format[-mirna.positions,]

## Export PCG table
mutated.TFBss.PCG.file <- file.path(results.dir, "Mutated_TFBSs_associated_to_PCGs.bed")
message("; Exporting table with mutated TFBSs associated to protein-coding genes: ", mutated.TFBss.PCG.file)
fwrite(mutated.TFBss.PCG, file = mutated.TFBss.PCG.file, sep = "\t", row.names = F, col.names = T, quote = FALSE)

## Export miRNA table
mutated.TFBss.miRNA.file <- file.path(results.dir, "Mutated_TFBSs_associated_to_miRNAs.bed")
message("; Exporting table with mutated TFBSs associated to miRNA genes:", mutated.TFBss.miRNA.file)
fwrite(mutated.TFBss.miRNA, file = mutated.TFBss.miRNA.file, sep = "\t", row.names = F, col.names = T, quote = FALSE)